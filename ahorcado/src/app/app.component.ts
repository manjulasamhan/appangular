import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

})
export class AppComponent {
  letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z']

  palabra = 'MURCIELAGO';
  palabraOculta = '';
  intentos = 0;

  gano = false;
  perdio = false;

  constructor() {

    for (let i = 0; i < this.palabra.length; i++) {


      this.palabraOculta = this.palabraOculta + '-';

    }


    console.log('Ya se creo ap.component.ts');
  }

  comprobar(letra) {
    this.existeLetra(letra);

    let palabraOcultaArr = this.palabraOculta.split('');
    for (let i = 0; i < this.palabra.length; i++) {
      if (this.palabra[i] === letra) {
        palabraOcultaArr[i] = letra;
      }
      this.palabraOculta = palabraOcultaArr.join('');

      this.verificaGana();
    }
  }
  existeLetra(letra) {

    if (this.palabra.indexOf(letra) > 0) {


    } else {
      if (this.intentos < 9)
        this.intentos++;
    }
  }
  verificaGana() {
    if (this.palabraOculta === this.palabra) {
      console.log('¡Usuario GANO!')
      this.gano = true;
    }
    if (this.intentos >= 9) {
      console.log('Usuario Perdio');
      this.perdio = true;
    }
   
  }
}